# Copyright (C) 2019 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

DESCRIPTION = "use cmake lib"
LICENSE = "MIT"
SECTION = "examples"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "git://gitlab.com/exempli-gratia/use-cmake-lib.git;protocol=https;branch=private-headers"

#SRCREV = "${AUTOREV}"
SRCREV = "89c82df125b2bd9a8aa1c8689985e55fb45eb909"

DEPENDS = "libhw-cmake-so-a"
DEPENDS += "libinternal-cmake-so-a"
RDEPENDS_${PN} = "libhw-cmake-so-a"
RDEPENDS_${PN} = "libinternal-cmake-so-a"

S = "${WORKDIR}/git"

inherit cmake

EXTRA_OECMAKE = ""
