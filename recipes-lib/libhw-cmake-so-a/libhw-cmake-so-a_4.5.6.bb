# Copyright (C) 2019 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

DESCRIPTION = "library which uses internal library - made with cmake"
LICENSE = "MIT"
SECTION = "examples"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

# we want our git repo:
SRC_URI = "git://gitlab.com/exempli-gratia/libhw-cmake-so-a.git;protocol=https;branch=private-headers;name=libhwcmake"
# we want the internal header file as well:
SRC_URI += "git://gitlab.com/exempli-gratia/libinternal-cmake-so-a.git;protocol=https;branch=private-headers;subpath=private;name=libinternalcmake" 

SRCREV_FORMAT = "libhwcmake_libinternalcmake"

#SRCREV_libhwcmake = "${AUTOREV}"
#SRCREV_libinternalcmake = "${AUTOREV}"

SRCREV_libhwcmake = "ced9ed5eb11d59d4670d61b38a0df6a19c14a2ed"
SRCREV_libinternalcmake = "902b99f4b59f751f2b741def32acbd744f818cc2"

DEPENDS = "libinternal-cmake-so-a"
RDEPENDS_${PN} = "libinternal-cmake-so-a"

# we might want this as well to explicitly say 
# that our giv SRCPV is bigger than version 0.0
#PV = "0.0+gitr${SRCPV}"

S = "${WORKDIR}/git"

inherit cmake

#EXTRA_OECMAKE += "-DLHWC_WITH_STATIC=ON -DLHWC_WITH_SHARED=ON"
EXTRA_OECMAKE += "-DLHWC_WITH_STATIC=OFF -DLHWC_WITH_SHARED=ON"

# we need the empty main pkg, as -staticdev depends on it => they end up in the SDK
# ALLOW_EMPTY_${PN} = "1"
#
