# Copyright (C) 2020 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

DESCRIPTION = "use libfoo.so.1.2.3 with makefile"
LICENSE = "MIT"
SECTION = "examples"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "git://gitlab.com/exempli-gratia/use-makefile-lib-so.git;protocol=https;branch=master" 
SRCREV = "${AUTOREV}"
PV = "1.2.3+git${SRCPV}"

# with this .h and .so files plus symlinks
# should be at the proper places to be found
DEPENDS = "libfoo-so-make"

# this is just for the package manager
# so libfoo-so-make is installed automatically
# if you install this testcase
RDEPENDS_${PN} = "libfoo-so-make"

# sources are under ${WORKDIR}/git <--
S = "${WORKDIR}/git"

# I want to pass this to my Makefile
EXTRA_OEMAKE = "DESTDIR=${D}${bindir}"

# all the work is done in the Makefile
# just creating dir and calling make install
do_install() {
        # create the dir if it does not already exists
        install -d ${D}${bindir}
        # install test case in there
        oe_runmake install
}

# just as a precaution against multi-core and crappy Makefiles
PARALLEL_MAKE=""

# Something like this is what we want:
# + oe-pkgdata-util list-pkg-files use-libfoo-so-make
# use-libfoo-so-make:
#        /usr/bin/foo_test
# + oe-pkgdata-util list-pkg-files use-libfoo-so-make-dev
# use-libfoo-so-make-dev:
# + oe-pkgdata-util list-pkg-files use-libfoo-so-make-dbg
# use-libfoo-so-make-dbg:
#        /usr/lib/debug/usr/bin/foo_test.debug
#        /usr/src/debug/use-libfoo-so-make/1.2.3+gitAUTOINC+5361aa56b7-r0/git/foo_test.c
# + oe-pkgdata-util list-pkg-files use-libfoo-so-make-src
# use-libfoo-so-make-src:
#
