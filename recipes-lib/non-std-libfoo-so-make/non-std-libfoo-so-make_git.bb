# Copyright (C) 2020 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

DESCRIPTION = "non std (libdir/include) libfoo.so.1.2.3 with makefile"
LICENSE = "MIT"
SECTION = "examples"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "git://gitlab.com/exempli-gratia/non-std-lib-makefile-so.git;protocol=https;branch=master" 
SRCREV = "${AUTOREV}"
PV = "1.2.3+git${SRCPV}"

# sources are under ${WORKDIR}/git <--
S = "${WORKDIR}/git"

# I want to pass those to my Makefile
EXTRA_OEMAKE =+ "includedir=${D}${includedir}/non-std libdir=${D}${libdir}/non-std"

# all the work is done in the Makefile
# just creating dirs and calling make install
do_install () {
    # create them in case they are not there
    install -d ${D}${includedir}
    install -d ${D}${libdir}
    # install
    oe_runmake install
}

# just as a precaution for crappy Makefiles and multi-core
PARALLEL_MAKE=""


#ERROR: non-std-libfoo-so-make-1.2.3+gitAUTOINC+672ffdbf5f-r0 do_package: QA Issue: non-std-libfoo-so-make: Files/directories were installed but not shipped in any package:
#  /usr/lib/non-std
#  /usr/lib/non-std/libnon-std-foo.so.1.2.3
#  /usr/lib/non-std/libnon-std-foo.so.1
#  /usr/lib/non-std/libnon-std-foo.so
#Please set FILES such that these items are packaged. Alternatively if they are unneeded, avoid installing them or delete them within do_install.
#non-std-libfoo-so-make: 4 installed and not shipped files. [installed-vs-shipped]
#ERROR: non-std-libfoo-so-make-1.2.3+gitAUTOINC+672ffdbf5f-r0 do_package: Fatal QA errors found, failing task.
#ERROR: Logfile of failure stored in: /workdir/build/multi-v7-ml-debug-training/tmp/work/armv7a-neon-resy-linux-gnueabi/non-std-libfoo-so-make/1.2.3+gitAUTOINC+672ffdbf5f-r0/temp/log.do_package.24534
#ERROR: Task (/workdir/sources/poky-training/../meta-lib/recipes-lib/non-std-libfoo-so-make/non-std-libfoo-so-make_git.bb:do_package) failed with exit code '1'

FILES_${PN} += "/usr/lib/non-std/lib*.so.* \
              "

FILES_${PN}-dev += "/usr/include/non-std \
                   /usr/lib/non-std/lib*.so \
                  "
               
