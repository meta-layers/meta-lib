##############################################################################################

package with std paths contents:

$ oe-pkgdata-util list-pkg-files libfoo-so-make-src
libfoo-so-make-src:

$ oe-pkgdata-util list-pkg-files libfoo-so-make-dbg
libfoo-so-make-dbg:
        /usr/lib/debug/usr/lib/libfoo.so.1.2.3.debug
        /usr/src/debug/libfoo-so-make/1.2.3+gitAUTOINC+efcb659db9-r0/git/foo.c

$ oe-pkgdata-util list-pkg-files libfoo-so-make-staticdev
libfoo-so-make-staticdev:

$ oe-pkgdata-util list-pkg-files libfoo-so-make-dev
libfoo-so-make-dev:
        /usr/include/foo.h
        /usr/lib/libfoo.so

$ oe-pkgdata-util list-pkg-files libfoo-so-make-doc
libfoo-so-make-doc:

$ oe-pkgdata-util list-pkg-files libfoo-so-make-locale
libfoo-so-make-locale:

$ oe-pkgdata-util list-pkg-files libfoo-so-make
libfoo-so-make:
        /usr/lib/libfoo.so.1
        /usr/lib/libfoo.so.1.2.3

-----

package with non-std paths content:

oe-pkgdata-util list-pkg-files non-std-libfoo-so-make-src
non-std-libfoo-so-make-src:

oe-pkgdata-util list-pkg-files non-std-libfoo-so-make-dbg
non-std-libfoo-so-make-dbg:
        /usr/lib/debug/usr/lib/non-std/libnon-std-foo.so.1.2.3.debug
        /usr/src/debug/non-std-libfoo-so-make/1.2.3+gitAUTOINC+672ffdbf5f-r0/git/non-std-foo.c

oe-pkgdata-util list-pkg-files non-std-libfoo-so-make-staticdev
non-std-libfoo-so-make-staticdev:

oe-pkgdata-util list-pkg-files non-std-libfoo-so-make-dev
non-std-libfoo-so-make-dev:
        /usr/include/non-std/non-std-foo.h
        /usr/lib/non-std/libnon-std-foo.so

oe-pkgdata-util list-pkg-files non-std-libfoo-so-make-doc
non-std-libfoo-so-make-doc:

oe-pkgdata-util list-pkg-files non-std-libfoo-so-make
non-std-libfoo-so-make:
        /usr/lib/non-std/libnon-std-foo.so.1
        /usr/lib/non-std/libnon-std-foo.so.1.2.3

-----

for building and running with std paths we care about:

$ oe-pkgdata-util list-pkg-files libfoo-so-make
libfoo-so-make:
        /usr/lib/libfoo.so.1
        /usr/lib/libfoo.so.1.2.3

$ oe-pkgdata-util list-pkg-files libfoo-so-make-dev
libfoo-so-make-dev:
        /usr/include/foo.h
        /usr/lib/libfoo.so

-----

for building and running with non-std paths we care about:

oe-pkgdata-util list-pkg-files non-std-libfoo-so-make
non-std-libfoo-so-make:
        /usr/lib/non-std/libnon-std-foo.so.1
        /usr/lib/non-std/libnon-std-foo.so.1.2.3

oe-pkgdata-util list-pkg-files non-std-libfoo-so-make-dev
non-std-libfoo-so-make-dev:
        /usr/include/non-std/non-std-foo.h
        /usr/lib/non-std/libnon-std-foo.so

-----
