# Copyright (C) 2019 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

DESCRIPTION = "internal library made with cmake"
LICENSE = "MIT"
SECTION = "examples"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "git://gitlab.com/exempli-gratia/libinternal-cmake-so-a.git;protocol=https;branch=private-headers"

SRCREV = "902b99f4b59f751f2b741def32acbd744f818cc2"
#SRCREV = "${AUTOREV}"

inherit cmake

S = "${WORKDIR}/git"
#EXTRA_OECMAKE += "-DLHWC_WITH_STATIC=ON -DLHWC_WITH_SHARED=ON"
EXTRA_OECMAKE += "-DLHWC_WITH_STATIC=OFF -DLHWC_WITH_SHARED=ON"

# we need the empty main pkg, as -staticdev depends on it => they end up in the SDK
# ALLOW_EMPTY_${PN} = "1"
#

# === external/internal SDK proof of concept ===

# !!!! Note !!!!
# Please note, that in order to get this to work also with the extensible SDK
# 1) the cmake file was adjusted since 
#    what we actually want is a PRIVATE header file 
#    which is somehow exposed to another library
# 2) the other library can use the private header file
#    via some recipe magic 
# 3) Looks like a single SDK might work - no external/internal SDK required
#
