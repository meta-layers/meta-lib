~/poky_sdk$ find | grep "internal-cmake\.h"

No "internal-cmake.h" file.

$ devtool extract libinternal-cmake-so-a /tmp/libinternal-cmake-so-a

$ tree /tmp/libinternal-cmake-so-a
/tmp/libinternal-cmake-so-a
├── CMakeLists.txt
├── internal-cmake1.c
├── internal-cmake2.c
└── private
    └── internal-cmake.h

1 directory, 4 files

... but ...

if I add to local.conf of extensible SDK:

BB_NO_NETWORK = "1" I get:

...

bb.data_smart.ExpansionError: Failure expanding variable SRCPV, expression was ${@bb.fetch2.get_srcrev(d)} which triggered exception NetworkAccess: Network access disabled through BB_NO_NETWORK (or set indirectly due to use of BB_FETCH_PREMIRRORONLY) but access requested with command git -c core.fsyncobjectfiles=0 ls-remote https://gitlab.com/exempli-gratia/libhw-cmake-so-a.git  (for url https://gitlab.com/exempli-gratia/libhw-cmake-so-a.git)

Please check on your setup.

The sources might be available somewhere in packages/sstate/dbg packages !
