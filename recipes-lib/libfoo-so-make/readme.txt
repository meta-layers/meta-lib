# This are the default dirs for the various packages:

bitbake libfoo-so-make -e | grep ^FILES\_
FILES_SOLIBSDEV="/lib/lib*.so /usr/lib/lib*.so"

FILES_libfoo-so-make="/usr/bin/* /usr/sbin/* /usr/libexec/* /usr/lib/lib*.so.*             
                      /etc /com /var /bin/* /sbin/*             
                      /lib/*.so.*             
                      /lib/udev /usr/lib/udev             
                      /lib/udev /usr/lib/udev             
                      /usr/share/libfoo-so-make /usr/lib/libfoo-so-make/*             
                      /usr/share/pixmaps /usr/share/applications             
                      /usr/share/idl /usr/share/omf /usr/share/sounds             
                      /usr/lib/bonobo/servers"

FILES_libfoo-so-make-bin="/usr/bin/* /usr/sbin/*"

FILES_libfoo-so-make-dbg="/usr/lib/debug /usr/src/debug"

FILES_libfoo-so-make-dev="/usr/include /lib/lib*.so /usr/lib/lib*.so /usr/lib/*.la                 
                          /usr/lib/*.o /usr/lib/pkgconfig /usr/share/pkgconfig                 
                          /usr/share/aclocal /lib/*.o                 
                          /usr/lib/libfoo-so-make/*.la /lib/*.la                 
                          /usr/lib/cmake /usr/share/cmake"

FILES_libfoo-so-make-doc="/usr/share/doc /usr/share/man /usr/share/info /usr/share/gtk-doc             
                          /usr/share/gnome/help"

FILES_libfoo-so-make-locale="/usr/share/locale"

FILES_libfoo-so-make-src=""

FILES_libfoo-so-make-staticdev="/usr/lib/*.a /lib/*.a /usr/lib/libfoo-so-make/*.a"

##############################################################################################

packages:

bitbake libfoo-so-make -e | grep ^PACKAGES=
PACKAGES="libfoo-so-make-src libfoo-so-make-dbg libfoo-so-make-staticdev libfoo-so-make-dev libfoo-so-make-doc libfoo-so-make-locale  libfoo-so-make"

##############################################################################################

package contents:

$ oe-pkgdata-util list-pkg-files libfoo-so-make-src
libfoo-so-make-src:

$ oe-pkgdata-util list-pkg-files libfoo-so-make-dbg
libfoo-so-make-dbg:
        /usr/lib/debug/usr/lib/libfoo.so.1.2.3.debug
        /usr/src/debug/libfoo-so-make/1.2.3+gitAUTOINC+efcb659db9-r0/git/foo.c

$ oe-pkgdata-util list-pkg-files libfoo-so-make-staticdev
libfoo-so-make-staticdev:

$ oe-pkgdata-util list-pkg-files libfoo-so-make-dev
libfoo-so-make-dev:
        /usr/include/foo.h
        /usr/lib/libfoo.so

$ oe-pkgdata-util list-pkg-files libfoo-so-make-doc
libfoo-so-make-doc:

$ oe-pkgdata-util list-pkg-files libfoo-so-make-locale
libfoo-so-make-locale:

$ oe-pkgdata-util list-pkg-files libfoo-so-make
libfoo-so-make:
        /usr/lib/libfoo.so.1
        /usr/lib/libfoo.so.1.2.3

-----

for building and running we care about:

$ oe-pkgdata-util list-pkg-files libfoo-so-make
libfoo-so-make:
        /usr/lib/libfoo.so.1
        /usr/lib/libfoo.so.1.2.3

$ oe-pkgdata-util list-pkg-files libfoo-so-make-dev
libfoo-so-make-dev:
        /usr/include/foo.h
        /usr/lib/libfoo.so


