# Copyright (C) 2020 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

DESCRIPTION = "libfoo.so.1.2.3 with makefile"
LICENSE = "MIT"
SECTION = "examples"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "git://gitlab.com/exempli-gratia/lib-makefile-so.git;protocol=https;branch=master" 
SRCREV = "${AUTOREV}"
PV = "1.2.3+git${SRCPV}"

# sources are under ${WORKDIR}/git <--
S = "${WORKDIR}/git"

# I want to pass those to my Makefile
EXTRA_OEMAKE =+ "includedir=${D}${includedir} libdir=${D}${libdir}"

# all the work is done in the Makefile
# just creating dirs and calling make install
do_install () {
    # create them in case they are not there
    install -d ${D}${includedir}
    install -d ${D}${libdir}
    # install
    oe_runmake install
}

# just as a precaution for crappy Makefiles and multi-core
PARALLEL_MAKE=""

# Something like this is what we want:

# + oe-pkgdata-util list-pkg-files libfoo-so-make
# libfoo-so-make:
#         /usr/lib/libfoo.so.1     (symlink) to /usr/lib/libfoo.so.1.2.3
#         /usr/lib/libfoo.so.1.2.3 (real file)

# + oe-pkgdata-util list-pkg-files libfoo-so-make-dev
# libfoo-so-make-dev:
#         /usr/include/foo.h       (real file)
#         /usr/lib/libfoo.so       (symlink) to /usr/lib/libfoo.so.1.2.3

