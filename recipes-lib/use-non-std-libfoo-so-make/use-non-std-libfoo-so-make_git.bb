# Copyright (C) 2020 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

DESCRIPTION = "use non-std (lib/include) libfoo.so.1.2.3 with makefile"
LICENSE = "MIT"
SECTION = "examples"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "git://gitlab.com/exempli-gratia/use-makefile-non-std-lib-so.git;protocol=https;branch=master" 
SRCREV = "${AUTOREV}"
PV = "1.2.3+git${SRCPV}"

# with this .h and .so files plus symlinks
# should be at the proper places to be found
DEPENDS = "non-std-libfoo-so-make"

# this is just for the package manager
# so libfoo-so-make is installed automatically
# if you install this testcase
RDEPENDS_${PN} = "non-std-libfoo-so-make"

# sources are under ${WORKDIR}/git <--
S = "${WORKDIR}/git"

# I want to pass this to my Makefile
EXTRA_OEMAKE = "DESTDIR=${D}${bindir} non-std-includedir=${STAGING_DIR_TARGET}/${includedir}/non-std non-std-libdir=${STAGING_DIR_TARGET}/${libdir}/non-std non-std-rpath=/usr/lib/non-std non-std-rpath-relative=../lib/non-std"

# all the work is done in the Makefile
# just creating dir and calling make install
do_install() {
        # create the dir if it does not already exists
        install -d ${D}${bindir}
        # install test case in there
        oe_runmake install
}

# just as a precaution against multi-core and crappy Makefiles
PARALLEL_MAKE=""

