We use an .so which installs .h and *.so.* into non default paths:

for building and running with non-std paths we care about:

oe-pkgdata-util list-pkg-files non-std-libfoo-so-make
non-std-libfoo-so-make:
        /usr/lib/non-std/libnon-std-foo.so.1
        /usr/lib/non-std/libnon-std-foo.so.1.2.3

oe-pkgdata-util list-pkg-files non-std-libfoo-so-make-dev
non-std-libfoo-so-make-dev:
        /usr/include/non-std/non-std-foo.h
        /usr/lib/non-std/libnon-std-foo.so

-----

We can build it quite easily, but in runtime the .so can no be found out of the box!

In order to get the exe to build I just passed this to my Makefile:

EXTRA_OEMAKE = "DESTDIR=${D}${bindir} non-std-includedir=${STAGING_DIR_TARGET}/${includedir}/non-std non-std-libdir=${STAGING_DIR_TARGET}/${libdir}/non-std"

-----

The problem we face with the YP/OE is, that we are cross-compiling and what's on the host (sysroot) and what's on the target rootfs is not in the same paths.

When a program is loaded the linker will check three places for library dependencies[1]:

*)  the directories specified in the executable's runtime library search path, which is compiled in during linking using the -rpath option.
*)  the directories specified in the library run path environment variable (LD_RUN_PATH).
*)  the list of libraries stored in /etc/ld.so.cache.

[1] https://web.archive.org/web/20110111043321/http://archive.itee.uq.edu.au/~daniel/using_origin/

This is from the YP mailing list: [2]

...

There are issues with playing with ld.so.conf when doing a cross image 
generation.  ldconfig doesn't always work during the rootfs process, most people 
don't notice since it's simply not necessary to be run in most cases!

...

Instead of using ldconfig, inform the application on the locations of the 
non-standard items.  This will then add them to the search path and ldconfig 
will no longer be necessary for a functional system, and instead become the 
optimization it was originally designed to be.

One way to do this, when you are repackaging something you didn't create and 
only have the binaries for, is to create a wrapper script that sets 
"LD_LIBRARY_PATH=<path to library>" and then executes the action binary..

or

If you are doing the compilation, use a built-in RPATH to specify where your 
application libraries exist.  See: http://itee.uq.edu.au/~daniel/using_origin/ 
for a good explanation of what to pass to the linker and how to use "$ORIGIN" to 
make it easier -- and your application relocatable on the target.  But in short 
you hard-code at compile time the location of your libraries based on the 
location of your executables.

[2] https://www.yoctoproject.org/pipermail/yocto/2011-November/003542.html


---------

1) LD_LIBRARY_PATH (only if you don't build it yourself - not nice)

1.1) build time

in the Makefile[3] those lines are active:

# for compilation in cooker mode:
# .h and .so files are already where they should be
# LD_LIBRARY_PATH=/usr/lib/non-std use-non-std-libfoo-so-make
$(EXENAME):
        $(CC) $(CFLAGS) $(LDFLAGS) $(EXENAME).c -o $@ -l$(LIBNAME) -L$(non-std-libdir) -I$(non-std-includedir)

we pass from the recipe:

EXTRA_OEMAKE = "DESTDIR=${D}${bindir} non-std-includedir=${STAGING_DIR_TARGET}/${includedir}/non-std non-std-libdir=${STAGING_DIR_TARGET}/${libdir}/non-std"

[3] https://gitlab.com/exempli-gratia/use-makefile-non-std-lib-so/-/blob/master/Makefile

1.2) run time

root@multi-v7-ml:~# use-non-std-libfoo-so-make 
use-non-std-libfoo-so-make: error while loading shared libraries: libnon-std-foo.so.1: cannot open shared object file: No such file or directory

root@multi-v7-ml:~# LD_LIBRARY_PATH=/usr/lib/non-std use-non-std-libfoo-so-make
Hello from non std foolib!

1.3) readelf

readelf -a use-non-std-libfoo-so-make

...

Dynamic section at offset 0xec4 contains 28 entries:
  Tag        Type                         Name/Value
 0x00000001 (NEEDED)                     Shared library: [libnon-std-foo.so.1]
 0x00000001 (NEEDED)                     Shared library: [libc.so.6]

...

2.1) rpath hardcoded

2.1.1) build time

in the Makefile[3] those lines are active:

# add hardcoded runpath into exe
$(EXENAME):
        $(CC) $(CFLAGS) $(LDFLAGS) $(EXENAME).c -o $@ -l$(LIBNAME) -L$(non-std-libdir) -I$(non-std-includedir) -Xlinker -R$(non-std-rpath)

we pass from the recipe:

EXTRA_OEMAKE = "DESTDIR=${D}${bindir} non-std-includedir=${STAGING_DIR_TARGET}/${includedir}/non-std non-std-libdir=${STAGING_DIR_TARGET}/${libdir}/non-std \
                non-std-rpath=/usr/lib/non-std"

2.1.2) run time

root@multi-v7-ml:~# use-non-std-libfoo-so-make 
Hello from non std foolib!

2.1.3) readelf

readelf -a use-non-std-libfoo-so-make

...

Dynamic section at offset 0xebc contains 29 entries:
  Tag        Type                         Name/Value
 0x00000001 (NEEDED)                     Shared library: [libnon-std-foo.so.1]
 0x00000001 (NEEDED)                     Shared library: [libc.so.6]
 0x0000000f (RPATH)                      Library rpath: [/usr/lib/non-std]

...

2.2) rpath with ORIGIN

2.2.1) build time

in the Makefile[3] those lines are active:

# add relative (to exe) runpath into exe
# in the YP this expands into an absolute path:
# readelf -a use-non-std-libfoo-so-make | grep rpath
# 0x0000000f (RPATH)                      Library rpath: [/usr/lib/non-std]
$(EXENAME):
        $(CC) $(CFLAGS) $(LDFLAGS) $(EXENAME).c -o $@ -l$(LIBNAME) -L$(non-std-libdir) -I$(non-std-includedir) -Wl,-rpath,'$$ORIGIN/$(non-std-rpath-relative)'

we pass from the recipe:

EXTRA_OEMAKE = "DESTDIR=${D}${bindir} non-std-includedir=${STAGING_DIR_TARGET}/${includedir}/non-std non-std-libdir=${STAGING_DIR_TARGET}/${libdir}/non-std \
                non-std-rpath-relative=../lib/non-std"

2.2.2) run time 

root@multi-v7-ml:~# use-non-std-libfoo-so-make 
Hello from non std foolib!

2.2.3) readelf

Dynamic section at offset 0xebc contains 29 entries:
  Tag        Type                         Name/Value
 0x00000001 (NEEDED)                     Shared library: [libnon-std-foo.so.1]
 0x00000001 (NEEDED)                     Shared library: [libc.so.6]
 0x0000000f (RPATH)                      Library rpath: [$ORIGIN/../lib/non-std]

Note that if you move the exe around it will not run anymore!

root@multi-v7-ml:/var/volatile/tmp# cp /usr/bin/use-non-std-libfoo-so-make /tmp
root@multi-v7-ml:/var/volatile/tmp# /tmp/use-non-std-libfoo-so-make 
/tmp/use-non-std-libfoo-so-make: error while loading shared libraries: libnon-std-foo.so.1: cannot open shared object file: No such file or directory
root@multi-v7-ml:/var/volatile/tmp# 

