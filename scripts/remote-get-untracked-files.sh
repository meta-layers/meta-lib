#!/bin/bash
source config.sh
set -x
cd /tmp
rm -rf untracked.tar.gz
scp rber@${LAB}:/tmp/untracked.tar.gz /tmp
set +x
