#!/bin/bash
source config.sh
set -x
cd /tmp
rm -rf modified.tar.gz
scp rber@${LAB}:/tmp/modified.tar.gz /tmp
set +x
