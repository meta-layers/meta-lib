# we use a core-image-minimal
require recipes-core/images/core-image-minimal.bb

# for testing include libs in image
#IMAGE_INSTALL += " libinternal-cmake-so-a "
#IMAGE_INSTALL += " libhw-cmake-so-a"

# we include the top level exe which calls lib, which calls (internal) lib
IMAGE_INSTALL += "use-cmake-lib"
# top level exe which should include the other as well
IMAGE_INSTALL += "use-libfoo-so-make"
IMAGE_INSTALL += "use-non-std-libfoo-so-make"
